// Your First Java Program
class MyClass {
	public static void main(String[ ] args) {
		System.out.println("Hello, World!");
	}
}
// The Main Method
public static void main(String[ ] args)
// Void Test
void test()
// System Out Print
{
   System.out.println("Hello World!");
}
// Challenge 1
public class Main {

   public static void main(String[] args) {
       System.out.println("Welcome to Java.");
   }
}
// Semicolons In Java
class MyClass {
	public static void main(String[ ] args)
	System.out.println("I am learning Java.");
}
// Comments
// This Is A Single Line Comment
/* This Is A Multi Line Comment */
// Variables
class MyClass {
	public static void main(String[ ] args) {
        var x = 5;
		System.out.println(x);
	}
}

